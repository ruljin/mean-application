const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const postsRoutes = require('./routes/posts');
const userRoutes = require('./routes/user');

const app = express();

mongoose.connect("mongodb+srv://fkuca:" + process.env.MONGO_ATLAS_PASSWORD + "@cluster0-4lwuv.mongodb.net/meanstack?retryWrites=true")
  .then(() => {
    console.log('Connected');
  })
  .catch(() => {
    console.log('Error');
  });

app.use(bodyParser.json());
app.use('/images', express.static(path.join('backend/images')));
// app.use(bodyParser.urlencoded());
// dodatkowa linia - deprecated

app.use((request, response, next) => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  response.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  response.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
  next();
})

app.use('/api/posts', postsRoutes);
app.use('/api/user', userRoutes);

module.exports = app;
